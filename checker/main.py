from portfolio import *

if __name__ == '__main__':
    ip = '127.0.0.1'
    flag = 'MY_FLAG'

    print('Check:')
    check_functionality(ip)
    print('Push:')
    domain = push_flag(ip, flag)
    print('Pull:')
    pull_flag(ip, domain, flag)
