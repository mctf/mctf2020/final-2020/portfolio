package ru.mctf.service

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.sessions.*
import io.ktor.util.pipeline.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import ru.mctf.AuthenticatedUser
import ru.mctf.dao.User
import ru.mctf.dao.Users
import ru.mctf.route.Paths
import java.nio.ByteBuffer
import java.sql.ResultSet
import java.time.LocalDateTime
import java.util.*


@Suppress("RemoveRedundantQualifierName")
class UserService(private val portfolioService: PortfolioService) {

    fun userExists(login: String): Boolean {
        return transaction {
            !Users.select { Users.login eq login }.empty()
        }
    }

    fun getUserById(id: UUID): User? {
        return transaction {
            Users.select { Users.id eq id }
                .limit(1)
                .map { toUser(it) }
                .getOrNull(0)
        }
    }

    fun getUser(login: String, password: String): User? {
        return transaction {
            Users.select { (Users.login eq login) and (Users.password like password) }
                .limit(1)
                .map { toUser(it) }
                .getOrNull(0)
        }
    }

    fun createUser(login: String, password: String, domain: String): UUID {
        val id = UUID.randomUUID()

        transaction {
            Users.insert {
                it[Users.id] = id
                it[Users.login] = login
                it[Users.password] = password
                it[Users.domain] = domain
                it[Users.registrationDate] = LocalDateTime.now()
            }
        }

        portfolioService.createPortfolio(id)

        return id
    }

    fun deleteUser(id: UUID) {
        transaction {
            Users.deleteWhere { Users.id eq id }
        }

        portfolioService.deletePortfolio(id)
    }

    fun getLatestUsers(): List<User> {
        return transaction {
            Users.selectAll()
                .orderBy(Users.registrationDate, SortOrder.DESC)
                .limit(100)
                .map { toUser(it) }
        }
    }

    fun getOutdatedUsers(): List<User> {
        val old = LocalDateTime.now().minusMinutes(10)
        return transaction {
            Users.select { Users.registrationDate less old }
                .map { toUser(it) }
        }
    }

    private fun toUser(res: ResultRow) =
        User(res[Users.id], res[Users.login], res[Users.password], res[Users.domain], res[Users.registrationDate])

    fun toUser(rs: ResultSet) = User(
        bytesToUUID(rs.getBytes(Users.id.name)),
        rs.getString(Users.login.name),
        rs.getString(Users.password.name),
        rs.getString(Users.domain.name),
        rs.getObject(Users.registrationDate.name, LocalDateTime::class.java)
    )

    private fun bytesToUUID(bytes: ByteArray): UUID {
        val bb: ByteBuffer = ByteBuffer.wrap(bytes)
        return UUID(bb.long, bb.long)
    }

}

suspend inline fun PipelineContext<Unit, ApplicationCall>.authenticateOrFail(
    userService: UserService,
    andThen: PipelineContext<Unit, ApplicationCall>.(User) -> Unit
) {
    tryAuthenticate(userService) { user ->
        if (user != null) {
            andThen(user)
        } else {
            call.respondRedirect(Paths.INDEX_PATH)
        }
    }
}

suspend inline fun PipelineContext<Unit, ApplicationCall>.tryAuthenticate(
    userService: UserService,
    andThen: PipelineContext<Unit, ApplicationCall>.(User?) -> Unit
) {
    val auth = call.sessions.get<AuthenticatedUser>()
    if (auth != null) {
        val user = userService.getUserById(auth.id)

        if (user == null) {
            call.respondRedirect(Paths.LOGOUT_PATH)
            return
        }

        andThen(user)
    } else {
        andThen(null)
    }
}