package ru.mctf.service

import org.jetbrains.exposed.sql.lowerCase
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import ru.mctf.dao.User
import ru.mctf.dao.Users
import java.math.BigInteger
import java.security.MessageDigest
import java.sql.ResultSet
import java.text.MessageFormat

class DomainService(private val userService: UserService, private val dnsService: DnsService) {

    private val namePattern = Regex("^[A-Za-z0-9]{3,32}$")

    val urlPattern = Regex("^(.{3,32})\\.portfolio\\.ctrl:3333$")

    fun createDomain(chosenName: String?, login: String): String {
        val name = chosenName ?: generateDomainName(login)
        dnsService.createDomain(name)
        return name
    }

    fun getDomainUser(name: String): User? {
        return transaction {
            MessageFormat("SELECT * FROM ${Users.nameInDatabaseCase()} WHERE LOWER(domain) = ''{0}''")
                .format(arrayOf(name.toLowerCase()))
                .execAndMap {
                    userService.toUser(it)
                }.getOrNull(0)
        }
    }

    private fun generateDomainName(login: String): String {
        var domain: String
        var counter = 0

        do {
            domain = (login + counter).md5()
            counter++
        } while (domainExists(domain))

        return domain
    }

    fun domainExists(name: String): Boolean {
        return transaction {
            !Users.select { Users.domain.lowerCase() eq name.toLowerCase() }.empty()
        }
    }

    fun validateDomain(name: String): Boolean {
        return name.matches(namePattern)
    }

    private fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }

    private fun <T : Any> String.execAndMap(transform: (ResultSet) -> T): List<T> {
        val result = arrayListOf<T>()
        TransactionManager.current().exec(this) { rs ->
            while (rs.next()) {
                result += transform(rs)
            }
        }
        return result
    }

}