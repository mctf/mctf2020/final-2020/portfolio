package ru.mctf.route

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import io.ktor.thymeleaf.*
import ru.mctf.AuthenticatedUser
import ru.mctf.service.UserService

@KtorExperimentalLocationsAPI
@Location(Paths.LOGIN_PATH)
class Login

@KtorExperimentalLocationsAPI
fun Route.login(userService: UserService) {
    get<Login> {
        val auth = call.sessions.get<AuthenticatedUser>()
        if (auth != null) {
            call.respondRedirect(Paths.INDEX_PATH)
        } else {
            val queryParams = call.request.queryParameters
            var errorMessage: String? = null

            if (queryParams.contains("incorrect")) {
                errorMessage = "Неправильный логин или пароль"
            }

            if (errorMessage != null) {
                call.respond(ThymeleafContent("login", mapOf("error" to errorMessage)))
            } else {
                call.respond(ThymeleafContent("login", emptyMap()))
            }
        }
    }

    post<Login> {
        val post = call.receive<Parameters>()
        val login = post["login"]
        val password = post["password"]

        if (login == null || password == null) {
            call.respondRedirect(Paths.LOGIN_PATH)
            return@post
        }

        val user = userService.getUser(login, password)

        if (user == null) {
            call.respondRedirect(Paths.LOGIN_PATH + "?incorrect")
            return@post
        }

        call.sessions.set(AuthenticatedUser(user.id))
        call.respondRedirect(Paths.INDEX_PATH)
    }
}