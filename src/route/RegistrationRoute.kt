package ru.mctf.route

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import io.ktor.thymeleaf.*
import ru.mctf.AuthenticatedUser
import ru.mctf.service.DomainService
import ru.mctf.service.UserService
import ru.mctf.service.tryAuthenticate

@KtorExperimentalLocationsAPI
@Location(Paths.REGISTER_PATH)
class Registration

val loginRegex = Regex("^[A-Za-z0-9_]{5,40}$")

@KtorExperimentalLocationsAPI
fun Route.registration(userService: UserService, domainService: DomainService) {
    get<Registration> {
        tryAuthenticate(userService) { user ->
            if (user != null) {
                call.respondRedirect(Paths.INDEX_PATH)
            } else {
                val queryParams = call.request.queryParameters
                var errorMessage: String? = null

                if (queryParams.contains("user_exists")) {
                    errorMessage = "Такой пользователь уже существует"
                } else if (queryParams.contains("domain_exists")) {
                    errorMessage = "Такой домен уже существует"
                }

                if (errorMessage != null) {
                    call.respond(ThymeleafContent("register", mapOf("error" to errorMessage)))
                } else {
                    call.respond(ThymeleafContent("register", emptyMap()))
                }
            }
        }
    }

    post<Registration> {
        val post = call.receive<Parameters>()
        val login = post["login"]
        val password = post["password"]
        val randomName = post["random_domain"] != null
        val chosenDomainName = if (randomName) null else post["domain"]

        if (!randomName && chosenDomainName == null) {
            call.respondRedirect(Paths.REGISTER_PATH)
            return@post
        }

        if (login == null || password == null) {
            call.respondRedirect(Paths.REGISTER_PATH)
            return@post
        }

        if (!login.matches(loginRegex) || password.length > 255) {
            call.respondRedirect(Paths.REGISTER_PATH)
            return@post
        }

        if (userService.userExists(login)) {
            call.respondRedirect(Paths.REGISTER_PATH + "?user_exists")
            return@post
        }

        if (chosenDomainName != null && !domainService.validateDomain(chosenDomainName)) {
            call.respondRedirect(Paths.REGISTER_PATH)
            return@post
        }

        if (chosenDomainName != null && domainService.domainExists(chosenDomainName)) {
            call.respondRedirect(Paths.REGISTER_PATH + "?domain_exists")
            return@post
        }

        val domainName = domainService.createDomain(chosenDomainName, login)
        val id = userService.createUser(login, password, domainName)

        call.sessions.set(AuthenticatedUser(id))
        call.respondRedirect(Paths.PORTFOLIO_EDIT_PATH)
    }
}