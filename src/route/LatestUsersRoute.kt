package ru.mctf.route

import io.ktor.application.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.thymeleaf.*
import ru.mctf.service.UserService
import ru.mctf.service.tryAuthenticate

@KtorExperimentalLocationsAPI
@Location(Paths.LATEST_PATH)
class LatestUsers

@KtorExperimentalLocationsAPI
fun Route.latestUsers(userService: UserService) {
    get<LatestUsers> {
        tryAuthenticate(userService) { user ->
            val latestUsers = userService.getLatestUsers()
            if (user != null) {
                call.respond(ThymeleafContent("latest", mapOf("user" to user, "latestUsers" to latestUsers)))
            } else {
                call.respond(ThymeleafContent("latest", mapOf("latestUsers" to latestUsers)))
            }
        }
    }
}