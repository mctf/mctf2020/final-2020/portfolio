package ru.mctf

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.*
import io.ktor.http.content.*
import io.ktor.locations.*
import io.ktor.routing.*
import io.ktor.sessions.*
import io.ktor.thymeleaf.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import org.thymeleaf.templateresolver.FileTemplateResolver
import ru.mctf.dao.Users
import ru.mctf.route.*
import ru.mctf.service.DnsService
import ru.mctf.service.DomainService
import ru.mctf.service.PortfolioService
import ru.mctf.service.UserService
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.set

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@KtorExperimentalLocationsAPI
@Suppress("unused")
fun Application.module() {
    install(Thymeleaf) {
        templateResolvers = setOf(
            FileTemplateResolver().apply {
                prefix = "./portfolios/"
                suffix = ""
                isCacheable = false
                checkExistence = true
                characterEncoding = "utf-8"
            },
            ClassLoaderTemplateResolver().apply {
                prefix = "templates/thymeleaf/"
                suffix = ".html"
                characterEncoding = "utf-8"
            }
        )
    }

    install(Locations)

    install(Sessions) {
        cookie<AuthenticatedUser>("session") {
            cookie.extensions["SameSite"] = "lax"
        }
    }

    initDatabase()

    val portfolioService = PortfolioService()
    val userService = UserService(portfolioService)
    val dnsService = DnsService()
    val domainService = DomainService(userService, dnsService)

    val cleanupTask = CleanupTask(userService, dnsService)
    val interval = TimeUnit.MINUTES.toMillis(5)
    Timer().schedule(cleanupTask, interval, interval)

    routing {
        index(userService)
        registration(userService, domainService)
        login(userService)
        logout()
        latestUsers(userService)
        portfolioEdit(userService, portfolioService)
        portfolioInterceptor(domainService)

        static("/static") {
            resources("static")
        }
    }
}

fun initDatabase() {
    val config = HikariConfig("/hikari.properties")
    val ds = HikariDataSource(config)
    Database.connect(ds)
    transaction {
        SchemaUtils.create(Users)
    }
}

data class AuthenticatedUser(val id: UUID)