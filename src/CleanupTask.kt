package ru.mctf

import ru.mctf.service.DnsService
import ru.mctf.service.UserService
import java.util.*

class CleanupTask(private val userService: UserService,
                  private val dnsService: DnsService) : TimerTask() {
    override fun run() {
        val outdatedUsers = userService.getOutdatedUsers()

        outdatedUsers.forEach {
            userService.deleteUser(it.id)
            dnsService.deleteDomain(it.domain)
        }
    }
}